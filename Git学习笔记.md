# Git学习笔记

## 一.安装git

使用以下命令设置用户名和邮箱

```sh
$ git config --global user.name "总钻风"
$ git config --global user.email "2508784442@qq.com"
```

--global参数表示本机所有git仓库都使用此配置

## 二.创建版本库

使用`git init`初始化版本库

![初始化版本库](images/git init.png)

使用`git add`添加文件到仓库

![git add](images/git add.png)

使用`git commit`提交文件, `-m`后面添加本次提交的说明

![git commit](images/git commit.png)

## 三.时光机穿梭

使用`git status`查看仓库当前状态, 使用命令`git config --global core.quotepath false`显示中文路径

![git status](images/git status.png)

使用`git diff`查看文件差异

![git diff](images/git diff.png)

### 1.版本回退

使用`git log`查看提交日志

![git log](images/git log.png)

使用`--pretty=oneline`参数,每个版本显示一行日志

![--pretty=oneline](images/git log --prett=oneline.png)

第一列的数字是Git的`commot id`, 在Git中, 用`HEAD`表示当前版本, 上一个版本是`HEAD^`, 上上个版本是`HEAD^^`, 往上N个版本可以写成`HEAD~N`

使用`git reset`进行版本回退

![git reset](images/git reset.png)

使用`git reset`回退到指定的版本

![回退指定版本](images/git reset 指定commit id.png)

使用`git reflog`查看命令历史记录

![git reflog](images/git reflog.png)

### 2.工作区和暂存区

**工作区(work dict)**

工作区就是电脑中能看到的目录, 比如`/d/workspace/workspace/learngitNew`

**版本库**

工作区中有一个隐藏目录`.git`, 此目录不算做工作区, 而是Git的版本库, Git版本库中保存有被称为`stage`(又称为`index`)的暂存区, 还有Git自动创建的分支`master`, 以及指向`master`分支的指针`HEAD`等

![工作区 暂存区 版本库关系](images/git structure.jpg)

向Git版本库中添加文件的两个步骤:

1. 用`git add`添加文件, 实际是将文件修改添加到暂存区
2. 用`git commit`提交文件, 实际是将文件

`git status`中三种状态的修改

- `Untracked files`: 未加入版本控制,未暂存状态
- `Changes not staged for commit`: 文件已修改, 未加入暂存区
- `Changes to be committed`: 文件修改已加入暂存区, 等待被提交

### 3.管理修改

==Git管理的是修改, 而不是文件==

必须先使用`git add`将修改加入暂存区再提交修改才会被提交到版本库.

`git diff`补充

- `git diff`: 工作区(work dict)和暂存区(stage)
- `git diff --cached`: 暂存区(stage)和版本库
- `git diff HEAD`: 工作区(work dict)和版本库

### 4.撤销修改

`git checkout -- file`(撤销文件在工作区的修改)(暂存区覆盖工作区)

```
$ git checkout -- LICENSE
```

- 文件修改后未添加到暂存区, 撤销修改会回到和版本库一致的状态
- 文件修改后已经添加到暂存区, 然后又做了修改, 撤销修改后会回到添加到暂存区时的状态

总之, ==文件回到最近一次`git commit`或`git add`后的状态==

`git reset -- file`(清空暂存区文件的修改操作)(版本库覆盖暂存区)

与`git reset HEAD file`作用相同

```sh
$ git reset -- LICENSE
```

### 5.删除文件

`git rm file`用于删除文件, 再使用`git commit`提交修改即可从版本库中删除文件,

也可以删除文件,再使用`git add`将修改添加到暂存区, 使用`gitcommit`提交

## 四.远程仓库

本地仓库和远程仓库之间通过ssh加密, 需要以下配置

1. 创建ssh key

   在Git Bash中执行以下命令

   ```powershell
   $ ssh-keygen -t rsa -C "youremail@example.com"
   ```

   ![生成SSH KEY](images/ssh.png)

2. 添加SSH KEY到远程仓库

   复制生成的`id_rsa.pub`公钥, 添加到gitee,https://gitee.com/profile/sshkeys

   ![添加SSH KEY](images/gitee ssh.png)

### 1.添加远程库

在gitee中新建一个项目, 根据提示运行以下命令将本地仓库关联到远程仓库

```
$ git remote add origin https://gitee.com/shily1995/gitlearnNew.git
```

添加后远程仓库的名字默认为`origin`

下一步, 把本地仓库所有内容推送到远程仓库

```
$ git push -u origin master
```

输入用户名密码即可提交,如果提示`remote: Incorrect username or password ( access token )`,说明密码错误, 在windows凭据管理器中修改为正确的用户名密码即可

![修改凭据](images/修改gitee的windows凭据.png)

![推送到远程仓库](images/git push第一次.png)

本地提交以后使用命令`git push origin master`即可推送本地仓库到远程仓库 

### 2.从远程库克隆

使用`git clone`克隆远程仓库到本地

![克隆远程库](images/git clone.png)

## 五.分支管理

### 1.创建于合并分支

**创建分支**

`HEAD`指向当前所在分支,创建`dev`分支时Git创建一个新的指针`dev`指向`master`, 再把`HEAD`指向`dev`

**合并分支**

当合并`dev`分支到`master上`, 直接将`master`指针指向`dev`当前的提交

---

使用`git checkout -b dev`创建并切换到`dev`分支

![创建分支并切换](images/git checkout -b dev.png)

`git checkout`命令加上`-b`参数表示创建并切换,相当于以下两条命令

```
$ git branch dev
$ git checkout dev
```

使用`git branch`查看当前分支, `git branch`会列出所有分支,并在当前分支前面加一个`*`

![查看当前分支](images/git branch.png)

在`dev`分支上修改文件, 提交,使用`git checkout master`切换到`master`分支, 然后合并到`master`分支

![合并分支](images/git merge dev.png)

删除`dev`分支

![删除dev分支](images/git branch -d dev.png)

### 2.解决冲突

当两个分支中的文件存在冲突时, 使用`git merge`会出现冲突

![合并分支冲突](images/git merge conflict.png)

查看LICENSE文件内容, Git使用`<<<<<<<`, `=======`, `>>>>>>>`标记不同分支的内容

![LICENSE文件内容](images/LICENSE文件内容.png)

修改之后再使用`git add`, `git commit`提交即可, 解决冲突就是把Git合并失败的文件手动编辑成我们希望的内容, 再提交.

使用带参数的`git log`可以看到分支的合并情况:

使用`git log --graph`可以看到分支合并图

```
$ git log --graph --pretty=oneline --abbrev-commit
```

![查看分支合并记录](images/git log查看分支合并记录.png)

### 3.分支管理策略

合并分支时, Git默认使用`Fast forward`模式 但在这种模式下, 删除分之后, 会丢掉分支信息

如果强制禁用`Fast Forward`模式, Git就会在merge时生成一个新的commit,这样, 从分支历史上就可以看出分支信息

分别在分支`dev`和`dev1`中进行两次提交, 分别使用在启用和禁用`Fase Forward`模式下进行合并

![合并前的历史信息](images/merge之前.png)

使用`Fase Forward`模式合并`dev`分支

![合并后的日志](images/合并后不删除dev分支.png)

禁用`Fast Forward`合并`dev1`分支

```
$ git merge --no-ff -m "MERGE with no-ff" dev1
```

![使用no-ff合并分之后的历史记录](images/使用no-ff方式合并.png)

使用`--no-ff`会产生一次新的commit, 所以要使用`-m`参数添加描述.

---

在实际开发中应该按照以下几个基本原则进行分支管理:

- `master`分支应该保持稳定, 仅用于发布新版本
- `dev`分支用于开发, 到某个时间将`dev`分支合并到`master`, 每个人都在自己的分支中开发,合并到`dev`分支即可

![团队合作分支示意图](images/团队合作分支.png)

### 4.BUG分支

修复bug通常采用以下方式, 为一个bug创建一个分支, 在此分支上修复bug, 修复后合并到`dev`分支, 然后删除临时分支

如果在修改文件后暂时无法提交, 同时又需要切换到其他分支, 可以使用`stash`功能存储当前工作现场, `git stash`会保存当前工作区和暂存区的修改, 但不会保存未加入版本库的文件, 此时再使用`git status`查看只有未添加到版本库的文件

![git stash](images/git stash.png)

使用`git stach list`查看所有的stash

![git stash list](images/git stash list.png)

使用`git stash apply`恢复, 恢复后使用`git stash drop`删除,

也可以使用`git stash pop`, 恢复后自动删除stash

可以进行多次stash, 可以恢复指定的stash, 使用以下命令

```
$ git stash apply stash@{0}:
```

### 5.feature分支

创建`feature-vulcan`分支用于开发新功能, 在`feature`分支上提交了修改, 但未合并到`dev`分支,此时使用`git branch -d feature-vulcan`无法删除`feature-vulcan `分支

![使用branch -d 删除feature分支](images/使用branch -d删除feature分支.png)

可以使用`-D`参数强行删除分支

![强制删除feature分支](images/使用branch -D强制删除feature分支.png)

### 6.多人协作

从远程仓库克隆时, Git会自动将本地的`master`分支和远程仓库的`master`分支关联起来, 远程仓库默认名称时`origin`

使用`git remote`命令查看远程仓库信息

![git remote](images/git remote.png)

使用`git remote -v`可以显示更详细的信息

![git remote -v](images/git remote -v.png)

详细信息中会显示可以抓取(`fetch`)和推送(`push`)的地址, 如果没有推送权限, 则没有推送地址

**推送分支**

推送分支时要指定本地分支, Git就会把该分支推送到远程仓库对应的远程分支上:

推送dev分支, 推送本地`dev`分支到远程仓库时, 如果远程仓库没有`dev`分支, 远程仓库会自动创建`dev`分支

```
$ git push origin dev
```

使用`git branch -a`可以查看远程仓库的分支

![git branch -a](images/git branch -a.png)

**抓取分支**

当从远程仓库克隆时, 默认情况下, 只能看到本地的`master`分支

![git branch](images/git clone后查看分支.png)

创建远程`origin`的`dev`分支到本地, 使用以下命令

```
$ git checkout -b dev origin/dev
```

![创建远程dev分支到本地](images/在本地创建远程仓库dev对应的dev分支.png)

多人协作时修改文件push到远程仓库时可能会出现以下问题, 是因为远程分支比本地分支版本新

![push冲突](images/git push冲突.png)

此时的解决方法是先使用`git pull`拉取远程仓库最新的提交, 在本地合并解决冲突后再推动到远程仓库

如果没有指定本地`dev`分支与远程`origin/dev`分支的连接, `git pull`不会成功

![git pull](images/未关联分支时git pull.png)

使用以下命令关联本地`dev`分支和远程仓库`origin/dev`分支

![关联远程分支和本地分支](images/关联远程分支和本地分支.png)

此时在进行`git pull`

![关联分支后Git pull](images/关联分支后Git pull.png)

在本地解决冲突后提交并推送到远程仓库即可.

![解决冲突后git push](images/解决冲突后git push.png)

总结, ==多人协作的工作模式通常是这样==:

1. 首先试图用`git push origin <branch>`推送自己的修改
2. 如果推送失败, 则因为远程分支比本地版本要新, 需要先使用`git pull`试图合并,如果`git pull`提示`no tracking information`, 则说明本地分支和远程分支为关联, 使用命令`git branch --set-upstream-to <branch> origin/<branch>`进行关联
3. 如果合并冲突, 则解决冲突, 并在本地提交
4. 没有冲突或解决冲突完成后, 再使用`git push origin <branch>`即可成功

### 7.Rebase

如果多个分支都进行了提交,在版本中分支会以分叉的形式前进,使用`rebase`可以将分叉的提交合并到同一条线上.

参考:[git rebase](http://gitbook.liuhui998.com/4_2.html)

## 六.标签管理

### 1.创建标签

- 打标签

	1. 切换到要加标签的分支
	2. 使用命令`git tag <tagname>`增加一个标签

- 使用命令`git tag`查看所有标签

- 默认标签打在最新的`commit id`上, 可以在命令最后加上commit id指定在某一个commit上打标签

- 使用`-a`指定标签名, `-m`指定说明文字
- 使用命令`git show <tagname>` 可以看到说明

### 2.操作标签

- 推送标签

  创建的标签都只存储在本地,不会自动推送到远程

  使用`git push origin <tagname>`推送某个标签到远程

  使用`git push origin --tags`推送所有未推送的本地标签		

- 删除标签

  使用`git tag -d <tagname>`可以删除本地标签

  删除远程标签要先从本地删除标签,再使用命令`git push origin :refs/tags/<tagname>`从远程仓库删除标签

## 七.自定义GIT

### 1.忽略特殊文件

在Git工作区的根目录下创建一个`.gitignore`文件, 在里面编写忽略文件的规则,Git就会忽略这些文件

**忽略文件的原则**:

1. 忽略操作系统自动生成的文件，比如缩略图等
2. 忽略编译生成的中间文件、可执行文件等，也就是如果一个文件是通过另一个文件自动生成的，那自动生成的文件就没必要放进版本库，比如Java编译产生的`.class`文件
3. 忽略你自己的带有敏感信息的配置文件，比如存放口令的配置文件

注:`.gitignore`文件本身要放到版本库中

### 2.配置命令别名

```
$ git config --global alias.st status
$ git config --global alias.co checkout
$ git config --global alias.ci commit
$ git config --global alias.br branch
$ git config --global alias.unstage 'reset HEAD'
$ git config --global alias.last 'log -1'
$ git config --global alias.lg "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
```

使用`--global`参数对当前用户起作用, 不加只针对当前仓库起作用

每个仓库的配置文件放在`.git/config`文件中,别名在`[alias]`后面, 删除行即可删除相应的别名

```
[core]
	repositoryformatversion = 0
	filemode = false
	bare = false
	logallrefupdates = true
	symlinks = false
	ignorecase = true
[remote "origin"]
	url = https://gitee.com/shily1995/gitlearnNew.git
	fetch = +refs/heads/*:refs/remotes/origin/*
[branch "master"]
	remote = origin
	merge = refs/heads/master
[branch "dev"]
	remote = origin
	merge = refs/heads/dev
[branch "tom"]
	remote = origin
	merge = refs/heads/tom
[alias]
	last = log -1
```

全局配置文件在用户主目录下的`.gitconfig`隐藏文件中

```
[user]
	name = 总钻风
	email = 2508784442@qq.com
[core]
	quotepath = false
[alias]
	lg = log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit
```

